# 快嘉框架脚手架

#### 介绍

学过Maven的人，都知道可以用eclipse的Maven插件生成一个项目骨架，比如maven-archetype-quickstart，这个原型就是最常用的脚手架之一。使用脚手架,可以快速启动从0到1，搭建好项目轮廓，甚至还有一些例子代码，提高了开发效率。

快嘉框架脚手架就是这样的一个脚手架，只不过这个脚手架是专门用来搭建框架框架的。


#### 使用说明

使用脚手架命令生成快嘉框架参考如下

当前版本：com.fastjrun.share:quickstart-archetype:1.0.1

```
mvn archetype:generate -U -B -DgroupId=com.fastjrun.share -DartifactId=demo -Dpackage="com.fastjrun.share.demo" -Dversion=1.0-SNAPSHOT -DarchetypeGroupId=com.fastjrun.share -DarchetypeArtifactId=quickstart-archetype -DarchetypeVersion=1.0.1 -Dauthor=fastjrun
```
上面命令生成的demo和[快嘉demo](https://gitee.com/fastjrun/demo "快嘉demo")除了README.md外，其他可是一模一样。

**可选构建参数**
- groupId
- artifactId  
- version  
- package 
